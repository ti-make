# config
IS_CONFIG =  yes

# architecture
PLAT =  linux_x86

# host
HOST =  linux

# root
PRO_DIR =  /home/enrich2/projects/personal/ti-make

# export
export PLAT
export HOST
export PRO_DIR
