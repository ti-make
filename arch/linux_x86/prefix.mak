# architecture makefile configure

# prefix & suffix
BIN_PREFIX 		= 
BIN_SUFFIX 		= 

OBJ_PREFIX 		= 
OBJ_SUFFIX 		= .o

LIB_PREFIX 		= lib
LIB_SUFFIX 		= .a

DLL_PREFIX 		= lib
DLL_SUFFIX 		= .so

ASM_SUFFIX 		= .s

# tool
PRE 			= 
CC 				= $(PRE)gcc
AR 				= $(PRE)ar -rc
LD 			= $(PRE)gcc
STRIP 			= $(PRE)strip
RANLIB 			= $(PRE)ranlib
RM 				= rm -f
RMDIR 			= rm -rf
CP 				= cp
CPDIR 			= cp -r
MKDIR 			= mkdir -p
MAKE 			= make
PWD 			= pwd

# cflags
CFLAGS 			= -O3 -g -Wall
CFLAGS-I 		= -I

# ldflags
LDFLAGS 		= 
LDFLAGS-L 		= -L
LDFLAGS-l 		= -l

# share ldflags
SHFLAGS 		= -shared -Wl,-soname

# include sub-config
include 		$(ARCH_DIR)$(_)config.mak


